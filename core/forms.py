__author__ = 'anuj'
from django import forms
from core.models import PinogyUser

class UserForm(forms.ModelForm):
    password1 = forms.CharField(min_length=6,max_length=32,required=False,widget=forms.widgets.PasswordInput(),
                                label="New Password")
    password2 = forms.CharField(min_length=6,max_length=32,required=False,widget=forms.widgets.PasswordInput(),
                                label="Repeat Password")

    class Meta:
        model = PinogyUser
        fields = ['first_name','last_name','email','phone','company_name','business_type']

    def clean(self):
        data = self.cleaned_data
        password1 = data.get('password1')
        password2 = data.get('password2')
        if password1 and not password2:
            raise forms.ValidationError("Repeat Password is mandatory to change password")
        if password2 and not password1:
            raise forms.ValidationError("New Password is mandatory to change password")
        if password1 and password2 and password1!=password2:
            raise forms.ValidationError("Passwords do not match. Please try again.")