from django.contrib import admin

# Register your models here.

from core.models import PinogyUser

class PinogyUserAdmin(admin.ModelAdmin):
    list_display = ('username','first_name','last_name','email','phone','company_name','business_type','date_joined','is_active')


admin.site.register(PinogyUser, PinogyUserAdmin)