from django.shortcuts import render_to_response, HttpResponse
from django.template import RequestContext
from core.forms import UserForm
from django.contrib.auth.decorators import login_required
import json
# Create your views here.

@login_required(login_url='/core/login/')
def user_profile(request,template_name):
    user = request.user
    if request.method == 'POST':
        user_form = UserForm(request.POST, instance=user)
        if user_form.is_valid():
            user_form_data = user_form.cleaned_data
            user_form.save()
            password = user_form_data.get('password1')
            if password:
                user.set_password(password)
                user.save()
            response_text = {
                "status":1,
                "message":"Data saved successfully"
            }
        else:
            error_list = []
            error_list.append(user_form.errors.as_ul())
            error_list.append(user_form.non_field_errors().as_ul())
            response_text = {
                "status":0,
                "message":"Error while saving data",
                "error": user_form.errors.as_ul()
            }
        return HttpResponse(json.dumps(response_text))
    else:
        user_form = UserForm(instance=user)
        return render_to_response(template_name, RequestContext(request,{'user_form':user_form}))
