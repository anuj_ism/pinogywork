from django.db import models
from django.contrib.auth.models import AbstractBaseUser, BaseUserManager, PermissionsMixin

# Create your models here.


class PinogyUserManager(BaseUserManager):
    """
        The manager for the auth user.
    """
    def create_user(self, username, password=None, **kwargs):
        user = self.model(
            username=username,
            first_name=kwargs.get('first_name'),
            last_name=kwargs.get('last_name'),
            phone=kwargs.get('phone'),
            email=kwargs.get('email'),
            company_name=kwargs.get('company_name'),
            business_type=kwargs.get('business_type'),
            is_superuser=False,
        )

        user.set_password(password)
        user.save(using=self._db)

        return user

    def create_superuser(self, username, password=None, **kwargs):
        """
        Creates and saves a superuser with the given username and other details
        """
        user = self.create_user(username, password, **kwargs)
        user.is_superuser = True
        user.is_staff = True
        user.save(using=self._db)
        return user


class PinogyUser(AbstractBaseUser, PermissionsMixin):
    """
        Custom user class
    """
    BUSINESS_DISTRIBUTOR = 1
    BUSINESS_MANUFACTURER = 2
    BUSINESS_RETAILER = 3

    BUSINESS_TYPES = (
        (BUSINESS_DISTRIBUTOR,"Distributor"),
        (BUSINESS_MANUFACTURER,"Manufacturer"),
        (BUSINESS_RETAILER,"Retailer"),
    )

    username = models.CharField(max_length=32, unique=True, null=False, blank=False)
    email = models.EmailField(null=True, blank=True)
    first_name = models.CharField(max_length=255, null=True, blank=True)
    last_name = models.CharField(max_length=255, null=True, blank=True)
    company_name = models.CharField(max_length=255, null=True, blank=True)
    business_type = models.PositiveSmallIntegerField(null=True, blank=True, choices=BUSINESS_TYPES)
    phone = models.CharField(max_length=10, null=True, blank=True)


    is_staff = models.BooleanField(default=False)
    date_joined = models.DateTimeField(auto_now_add=True)
    is_active = models.BooleanField(default=True)

    updated_at = models.DateTimeField(auto_now=True)

    objects = PinogyUserManager()

    USERNAME_FIELD = 'username'
    REQUIRED_FIELDS = []

    def __unicode__(self):
        return "%s" % self.username

    def __str__(self):
        return "%s" % self.username

    def get_full_name(self):
        return '%s %s' % (self.first_name,self.last_name)

    def get_short_name(self):
        return self.first_name
