__author__ = 'anuj'
from django.conf.urls import url
import views

urlpatterns = [
    # Examples:
    # url(r'^$', 'pinogywork.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    url(r'^user-profile/$', views.user_profile, {'template_name':'user-profile.html'}, name="core_userprofile")
]