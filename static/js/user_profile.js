/**
 * Created by anuj on 6/2/16.
 */

$("#user-form").on("submit", function(event){
    event.preventDefault();
    create_post();
});

function create_post(){
    var data = $("#user-form").serializeArray();
    $("#errorDiv").html("");
    $("#errorDiv").hide();
    $("#successDiv").html("");
    $("#successDiv").hide();
    $.ajax(
        {
            url:"/core/user-profile/",
            type:"POST",
            data:data,

            success: function (response) {
                var result = JSON.parse(response);

                $("#id_password1").val("");
                $("#id_password2").val("");

                $('.body')[0].scrollIntoView(true);

                if(result["status"]==0){
                    $("#errorDiv").show();
                    $("#errorDiv").html('<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>');
                    $("#errorDiv").append(result["error"]);
                }
                else{
                    $("#successDiv").show();
                    $("#successDiv").html('<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>');
                    $("#successDiv").append(result["message"]);
                }
            },

            error: function(xhr,errorMsg,err){
                console.log(xhr.status+""+xhr.responseText);
                $("#id_password1").val("");
                $("#id_password2").val("");
                alert(errorMsg);
            }
        }
    );
}